import './App.css'
import Header from './components/Header'
import HeaderClass from './components/HeaderClassComponent'
const data ="Shabeer";
const data2 = "Class component"

function App() {
  return (
    <div className="App">
      <Header data={data}/>
      <Desc/>
      <HeaderClass data2={data2}/>
    </div>
  );
}

export default App;

function Desc (){
  return(
    <p>This is description component</p>
  )
}